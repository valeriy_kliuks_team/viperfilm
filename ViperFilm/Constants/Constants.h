//
//  Constants.h
//  ViperFilm
//
//  Created by Valeriy Kliuk on 2016-02-05.
//  Copyright © 2016 R3D Numerique. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

static NSString * const AFAppDDBMoviesDeveloperAPIBaseURLString = @"https://api.themoviedb.org/3";
static NSString * const AFImageTMDBAPIBaseURLString = @"http://image.tmdb.org/t/p/w";


#endif /* Constants_h */
