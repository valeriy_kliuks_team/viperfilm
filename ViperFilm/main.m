//
//  main.m
//  ViperFilm
//
//  Created by Valeriy Kliuk on 2016-01-31.
//  Copyright © 2016 R3D Numerique. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
