//
// Created by VIPER
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilmEditProtocols.h"

@interface FilmEditView : UIViewController <FilmEditViewProtocol>

@property (nonatomic, strong) id <FilmEditPresenterProtocol> presenter;

@end
