//
// Created by VIPER
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol FilmEditInteractorOutputProtocol;
@protocol FilmEditInteractorInputProtocol;
@protocol FilmEditViewProtocol;
@protocol FilmEditPresenterProtocol;
@protocol FilmEditLocalDataManagerInputProtocol;
@protocol FilmEditAPIDataManagerInputProtocol;


@class FilmEditWireFrame;

@protocol FilmEditViewProtocol
@required
@property (nonatomic, strong) id <FilmEditPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
@end

@protocol FilmEditWireFrameProtocol
@required
+ (void)presentFilmEditModuleFrom:(id)fromView;
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
@end

@protocol FilmEditPresenterProtocol
@required
@property (nonatomic, weak) id <FilmEditViewProtocol> view;
@property (nonatomic, strong) id <FilmEditInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <FilmEditWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
@end

@protocol FilmEditInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

@protocol FilmEditInteractorInputProtocol
@required
@property (nonatomic, weak) id <FilmEditInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <FilmEditAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <FilmEditLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
@end


@protocol FilmEditDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol FilmEditAPIDataManagerInputProtocol <FilmEditDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
@end

@protocol FilmEditLocalDataManagerInputProtocol <FilmEditDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end


