//
// Created by VIPER
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FilmEditProtocols.h"


@interface FilmEditInteractor : NSObject <FilmEditInteractorInputProtocol>

@property (nonatomic, weak) id <FilmEditInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <FilmEditAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <FilmEditLocalDataManagerInputProtocol> localDataManager;

@end