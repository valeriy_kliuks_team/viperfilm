//
// Created by VIPER
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FilmEditProtocols.h"
#import "FilmEditView.h"
#import "FilmEditLocalDataManager.h"
#import "FilmEditAPIDataManager.h"
#import "FilmEditInteractor.h"
#import "FilmEditPresenter.h"
#import "FilmEditWireframe.h"
#import <UIKit/UIKit.h>

@interface FilmEditWireFrame : NSObject <FilmEditWireFrameProtocol>

@end
