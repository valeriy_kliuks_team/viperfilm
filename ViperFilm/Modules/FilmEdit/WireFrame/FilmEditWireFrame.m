//
// Created by VIPER
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import "FilmEditWireFrame.h"

@implementation FilmEditWireFrame

+ (void)presentFilmEditModuleFrom:(UIViewController*)fromViewController
{
    // Generating module components
    id <FilmEditViewProtocol> view = [[FilmEditView alloc] init];
    id <FilmEditPresenterProtocol, FilmEditInteractorOutputProtocol> presenter = [FilmEditPresenter new];
    id <FilmEditInteractorInputProtocol> interactor = [FilmEditInteractor new];
    id <FilmEditAPIDataManagerInputProtocol> APIDataManager = [FilmEditAPIDataManager new];
    id <FilmEditLocalDataManagerInputProtocol> localDataManager = [FilmEditLocalDataManager new];
    id <FilmEditWireFrameProtocol> wireFrame= [FilmEditWireFrame new];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    //TOODO - New view controller presentation (present, push, pop, .. )
}

@end
