//
// Created by VIPER
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FilmEditProtocols.h"

@class FilmEditWireFrame;

@interface FilmEditPresenter : NSObject <FilmEditPresenterProtocol, FilmEditInteractorOutputProtocol>

@property (nonatomic, weak) id <FilmEditViewProtocol> view;
@property (nonatomic, strong) id <FilmEditInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <FilmEditWireFrameProtocol> wireFrame;

@end
