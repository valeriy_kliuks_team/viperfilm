//
// Created by VIPER
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FilmProtocols.h"
#import "FilmView.h"
#import "FilmLocalDataManager.h"
#import "FilmAPIDataManager.h"
#import "FilmInteractor.h"
#import "FilmPresenter.h"
#import "FilmWireframe.h"
#import <UIKit/UIKit.h>

@interface FilmWireFrame : NSObject <FilmWireFrameProtocol>

@end
