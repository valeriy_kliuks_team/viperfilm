//
// Created by VIPER
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import "FilmWireFrame.h"

@implementation FilmWireFrame

+ (void)presentFilmModuleFrom:(UIViewController*)fromViewController
{
    // Generating module components
    id <FilmViewProtocol> view = [[FilmView alloc] init];
    id <FilmPresenterProtocol, FilmInteractorOutputProtocol> presenter = [FilmPresenter new];
    id <FilmInteractorInputProtocol> interactor = [FilmInteractor new];
    id <FilmAPIDataManagerInputProtocol> APIDataManager = [FilmAPIDataManager new];
    id <FilmLocalDataManagerInputProtocol> localDataManager = [FilmLocalDataManager new];
    id <FilmWireFrameProtocol> wireFrame= [FilmWireFrame new];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    
    //TOODO - New view controller presentation (present, push, pop, .. )
}

+ (id <FilmViewProtocol>)generateFilmModule
{
    // Generating module components
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    id <FilmViewProtocol> view = [storyboard instantiateViewControllerWithIdentifier:@"FilmView"];
    id <FilmPresenterProtocol, FilmInteractorOutputProtocol> presenter = [FilmPresenter new];
    id <FilmInteractorInputProtocol> interactor = [FilmInteractor new];
    id <FilmAPIDataManagerInputProtocol> APIDataManager = [FilmAPIDataManager new];
    id <FilmLocalDataManagerInputProtocol> localDataManager = [FilmLocalDataManager new];
    id <FilmWireFrameProtocol> wireFrame= [FilmWireFrame new];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;
    interactor.localDataManager = localDataManager;
    return view;
}

@end
