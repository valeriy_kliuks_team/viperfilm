//
// Created by VIPER
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import "FilmInteractor.h"

@implementation FilmInteractor

- (void)findUpcomingItems
{
    __weak typeof(self) welf = self;
    [self.APIDataManager getFilmsTopRatedWithResultBlock:^(NSArray *films) {
        [welf.presenter foundUpcomingItems:films];
    } failedBlock:^{
        [welf.presenter foundUpcomingItems:nil];
    }];
}

@end