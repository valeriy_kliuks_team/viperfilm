//
// Created by VIPER
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FilmProtocols.h"

@class FilmWireFrame;

@interface FilmPresenter : NSObject <FilmPresenterProtocol, FilmInteractorOutputProtocol>

@property (nonatomic, weak) id <FilmViewProtocol> view;
@property (nonatomic, strong) id <FilmInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <FilmWireFrameProtocol> wireFrame;

@end
