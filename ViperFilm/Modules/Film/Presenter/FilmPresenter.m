//
// Created by VIPER
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import "FilmPresenter.h"
#import "FilmWireframe.h"

@implementation FilmPresenter

- (void)updateView
{
    [self.interactor findUpcomingItems];
}

- (void)foundUpcomingItems:(NSArray *)upcomingItems{
    [self updateUserInterfaceWithUpcomingItems:upcomingItems];
}

- (void)updateUserInterfaceWithUpcomingItems:(NSArray *)upcomingItems
{
    [self.view showUpcomingDisplayData:upcomingItems];
}


@end