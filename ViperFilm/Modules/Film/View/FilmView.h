//
// Created by VIPER
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilmProtocols.h"

@interface FilmView : UITableViewController <FilmViewProtocol>

@property (nonatomic, strong) id <FilmPresenterProtocol> presenter;
@property (strong, nonatomic) IBOutlet UIView *noContentView;

@end
