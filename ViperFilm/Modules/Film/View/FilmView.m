//
// Created by VIPER
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import "FilmView.h"
#import "FilmItem.h"

@interface FilmView ()

@property (nonatomic, strong) NSArray *films;

@end


static NSString *filmCellIdentifier = @"FilmCellIdentifier";

@implementation FilmView

#pragma mark - ViewController Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:filmCellIdentifier];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.presenter updateView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

#pragma mark - UITableViewDelegate and DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.films count];
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:filmCellIdentifier forIndexPath:indexPath];
    
    FilmItem *f = [self.films objectAtIndex:indexPath.row];
    cell.textLabel.text = f.title;
    cell.detailTextLabel.text = @"";
    return cell;
}

-(void)showUpcomingDisplayData:(NSArray *)data{
    self.films = [NSArray array];
    if (data) {
        self.films = [NSArray arrayWithArray:data];
    }
    [self.tableView reloadData];
}

@end