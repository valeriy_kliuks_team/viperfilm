//
// Created by VIPER
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol FilmInteractorOutputProtocol;
@protocol FilmInteractorInputProtocol;
@protocol FilmViewProtocol;
@protocol FilmPresenterProtocol;
@protocol FilmLocalDataManagerInputProtocol;
@protocol FilmAPIDataManagerInputProtocol;


@class FilmWireFrame, FilmView;

@protocol FilmViewProtocol
@required
@property (nonatomic, strong) id <FilmPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
- (void)showUpcomingDisplayData:(NSArray *)data;

@end

@protocol FilmWireFrameProtocol
@required
+ (void)presentFilmModuleFrom:(id)fromView;
+ (id <FilmViewProtocol>)generateFilmModule;
/**
 * Add here your methods for communication PRESENTER -> WIREFRAME
 */
@end

@protocol FilmPresenterProtocol
@required
@property (nonatomic, weak) id <FilmViewProtocol> view;
@property (nonatomic, strong) id <FilmInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <FilmWireFrameProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER -> PRESENTER
 */
- (void)updateView;

@end

@protocol FilmInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
- (void)foundUpcomingItems:(NSArray *)upcomingItems;

@end

@protocol FilmInteractorInputProtocol
@required
@property (nonatomic, weak) id <FilmInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <FilmAPIDataManagerInputProtocol> APIDataManager;
@property (nonatomic, strong) id <FilmLocalDataManagerInputProtocol> localDataManager;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
- (void)findUpcomingItems;

@end


@protocol FilmDataManagerInputProtocol
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

@protocol FilmAPIDataManagerInputProtocol <FilmDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> APIDATAMANAGER
 */
- (void)getFilmsTopRatedWithResultBlock:(void(^)(NSArray *films))resultBlock failedBlock:(void(^)(void))failedBlock;

@end

@protocol FilmLocalDataManagerInputProtocol <FilmDataManagerInputProtocol>
/**
 * Add here your methods for communication INTERACTOR -> LOCLDATAMANAGER
 */
@end


