//
// Created by VIPER
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import "FilmAPIDataManager.h"
#import "AFNetworking.h"
#import "FilmItem.h"

static NSString * const AFAppDDBMoviesDeveloperAPIBaseURLString = @"https://api.themoviedb.org/3";

@implementation FilmAPIDataManager

-(void)getFilmsTopRatedWithResultBlock:(void (^)(NSArray *))resultBlock failedBlock:(void (^)(void))failedBlock{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", AFAppDDBMoviesDeveloperAPIBaseURLString, @"/movie/top_rated?api_key=b1bb220e43edab243b3265350a8dce9e"];
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSDictionary *responseDictionary = (NSDictionary *) responseObject;
        NSMutableArray *films = [NSMutableArray new];
        [responseDictionary[@"results"] enumerateObjectsUsingBlock:^(NSDictionary *d, NSUInteger idx, BOOL *stop) {
            FilmItem *f = [[FilmItem alloc] initWithTitle:d[@"title"]];
            [films addObject:f];
        }];
        if (resultBlock) {
            resultBlock(films);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (failedBlock) {
            failedBlock();
        }
    }];
}

@end