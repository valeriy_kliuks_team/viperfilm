//
// Created by VIPER.
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FilmProtocols.h"

@class FilmItem;

@interface FilmAPIDataManager : NSObject <FilmAPIDataManagerInputProtocol>

@end