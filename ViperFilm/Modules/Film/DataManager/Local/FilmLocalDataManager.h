//
// Created by VIPER
// Copyright (c) 2016 VIPER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FilmProtocols.h"


@interface FilmLocalDataManager : NSObject <FilmLocalDataManagerInputProtocol>

@end