//
//  ViewController.m
//  ViperFilm
//
//  Created by Valeriy Kliuk on 2016-01-31.
//  Copyright © 2016 R3D Numerique. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
