//
//  ViperFilmTests.m
//  ViperFilmTests
//
//  Created by Valeriy Kliuk on 2016-01-31.
//  Copyright © 2016 R3D Numerique. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FilmAPIDataManager.h"

@interface ViperFilmTests : XCTestCase

@property (nonatomic, strong) FilmAPIDataManager *webService;

@end

@implementation ViperFilmTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.webService = [FilmAPIDataManager new];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    self.webService = nil;
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

- (void)testWebService {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    
    XCTestExpectation *webServiceExpectation = [self expectationWithDescription:@"web service called"];
    
    [self.webService getFilmsTopRatedWithResultBlock:^(NSArray *films) {
        NSLog(@"Films on the page:%lu", (unsigned long)films.count);
        XCTAssert(YES);
        [webServiceExpectation fulfill];
    } failedBlock:^{
        XCTAssert(NO);
        [webServiceExpectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        
    }];
}


@end
